"use strict";
// function add(n1: number , n2:number){
//     return n1 + n2;
// }
// function printvalue(num: number){
//     console.log('Result :' + num);
// }
// printvalue(add(2,4));
// function type
// function add(n1: number , n2:number){
//     return n1 + n2;
// }
// function printvalue(num: number){
//     console.log('Result :' + num);
// }
// printvalue(add(2,4));
// let combineValue: (a: number , b: number) => number;
// //  combineValue function should have two agument  both are number type, and that will reture a number.
// combineValue = add;
// // combineValue = printvalue;
// console.log(combineValue(2,2));
/* Call back in function type */
function add(n1, n2) {
    return n1 + n2;
}
function printvalue(num) {
    console.log('Result :' + num);
}
function addAndHandle(n1, n2, cb) {
    var result = n1 + n2;
    cb(result);
}
printvalue(add(2, 4));
var combineValue;
//  combineValue function should have two agument  both are number type, and that will reture a number.
combineValue = add;
// combineValue = printvalue;
console.log(combineValue(2, 2));
addAndHandle(10, 5, function (result) {
    console.log(result);
});
