"use strict";
// union Type
function combine(inp1, inp2, resultConversion) {
    var result;
    if (typeof inp1 == 'number' && typeof inp2 == 'number' || resultConversion == 'is-number') {
        result = +inp1 + +inp2;
    }
    else {
        result = inp1.toString() + inp2.toString();
    }
    return result;
}
var combinedAges = combine(10, 20, 'is-number');
console.log(combinedAges);
var combinedNames = combine('Abhilash', ' P B', 'is-text');
console.log(combinedNames);
var combinedTextedAges = combine('10', '20', 'is-number');
console.log(combinedTextedAges);
