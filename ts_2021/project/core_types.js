"use strict";
// const person = {
//     name: 'Abhilash',
//     age: 25
// }
// console.log(person);
var Role;
(function (Role) {
    Role[Role["ADMIN"] = 0] = "ADMIN";
    Role[Role["READ_ONLY"] = 1] = "READ_ONLY";
    Role[Role["AUTHER"] = 2] = "AUTHER";
})(Role || (Role = {}));
;
var person = {
    name: 'Abhilash',
    age: 25,
    role: Role.ADMIN
};
console.log(person);
if (person.role == Role.ADMIN) {
    console.log('is Admin');
}
