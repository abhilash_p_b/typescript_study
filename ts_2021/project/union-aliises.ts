
// union Type

// function combine(inp1: number | string, inp2: number | string) {
//     let result;
//     if( typeof inp1 == 'number' && typeof inp2 == 'number'){
//         result = inp1 + inp2;
//         return result;
//     }else{
//         result = inp1.toString() + inp2.toString();
//         return result;  
//     }

// }
// const combinedAges = combine(10, 20);
// console.log(combinedAges);

// const combinedNames = combine('Abhilash',' P B');
// console.log(combinedNames);

// litral Type
// function combine(inp1: number | string, inp2: number | string, resultConversion: string) {
//     let result;
//     if (typeof inp1 == 'number' && typeof inp2 == 'number' || resultConversion == 'is-number') {
//         result = +inp1 + +inp2;
//     } else {
//         result = inp1.toString() + inp2.toString();
//     }
//     return result;
// }
// const combinedAges = combine(10, 20, 'is-number');
// console.log(combinedAges);

// const combinedNames = combine('Abhilash', ' P B', 'is-text');
// console.log(combinedNames);

// const combinedTextedAges = combine('10', '20', 'is-number');
// console.log(combinedTextedAges);

// type

type combine = number | string;

function combine(inp1: combine, inp2: combine, resultConversion: string) {
    let result;
    if (typeof inp1 == 'number' && typeof inp2 == 'number' || resultConversion == 'is-number') {
        result = +inp1 + +inp2;
    } else {
        result = inp1.toString() + inp2.toString();
    }
    return result;
}
const combinedAges = combine(10, 20, 'is-number');
console.log(combinedAges);

const combinedNames = combine('Abhilash', ' P B', 'is-text');
console.log(combinedNames);

const combinedTextedAges = combine('10', '20', 'is-number');
console.log(combinedTextedAges);