let userInput: unknown;
let userName: string;

userInput = 5;
userInput = ' Abhi';

if (typeof userInput == 'string') {
    userName = userInput
}
// userName = userInput; ->  // if userInput is type `unknown` it is not equal but in case
                      //userInput is `any` then it will accept. 
                      // `unknown` is better than `any`.

/* never type */

function generateError(message: string, code: number):never{
    throw {message: message, code: code};
}

generateError('Internal Error!', 500);
