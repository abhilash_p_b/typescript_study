// interfaces

import { Invoice } from './classes/Invoice.js';
import { Payment } from './classes/Payment.js';
import { HasFormatter } from './interfaces/HasFormatter.js';
import { ListTemplate } from './classes/ListTemplate.js';

console.log('App.ts');

const inv1 = new Invoice('abhi', 'details', 100);

const select = document.querySelector('#select') as HTMLSelectElement;
const fromTo = document.querySelector('#fromTo') as HTMLInputElement;
const details = document.querySelector('#details') as HTMLInputElement;
const amt = document.querySelector('#amt') as HTMLInputElement;

const button = document.querySelector('#btn')!;

const ul = document.querySelector('ul')!;
const list = new ListTemplate(ul);
button.addEventListener("click", (e: Event) => {
    e.preventDefault();
    let doc: HasFormatter;
    console.log(select.value);
    if (select.value === 'Invoice') {
        doc = new Invoice(fromTo.value, details.value, +amt.value);
    } else {
        doc = new Payment(fromTo.value, details.value, +amt.value);
    }
    // console.log(doc)
    list.render(doc, details.value, 'end');

});

// GENERICS

// 1st
// const addUID = (obj: object) => {
//     let uid = Math.floor(Math.random() * 100);
//     return { ...obj, uid };
// }

// let docOne = addUID({ name: 'abhi', age: 25 });

// // console.log(docOne.name); // which generate an error
// console.log(docOne);

// 2nd
// const addUID = <T extends object>(obj: T) => {
//     let uid = Math.floor(Math.random() * 100);
//     return { ...obj, uid };
// }

// let docOne = addUID({ name: 'abhi', age: 25 });
// let docTwo = addUID('test'); /// by using  extends we can define the type. 

// console.log(docOne.name);
// //  so this object structure is corresponding to the return object.

/* using intrefaces */

const addUID = <T extends object>(obj: T) => {
    let uid = Math.floor(Math.random() * 100);
    return { ...obj, uid };
}

let docOne = addUID({ name: 'abhi', age: 25 });
// let docTwo = addUID('test'); 

console.log(docOne.name);

interface Resources<T>{
    uid: number,
    resourceName: string,
    date: T
}

let codeThree: Resources<object> = {
    uid: 1,
    resourceName: 'user',
    date: {name:'abhi'}
}
let codeFour: Resources<string[]> = {
    uid: 2,
    resourceName: 'user test',
    date: ['yy','xx']
}
console.log(codeThree,codeFour)

//END
