import {HasFormatter} from '../interfaces/HasFormatter.js'
 export class Payment implements HasFormatter {
    constructor(
        readonly reci: string,
        private details: string,
        public amount: number
    ){}

    format(){
        return `${this.reci} , amount ${this.amount} ${this.details}`;
    }
}
