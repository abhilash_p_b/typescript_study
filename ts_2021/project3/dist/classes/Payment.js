export class Payment {
    constructor(reci, details, amount) {
        this.reci = reci;
        this.details = details;
        this.amount = amount;
    }
    format() {
        return `${this.reci} , amount ${this.amount} ${this.details}`;
    }
}
